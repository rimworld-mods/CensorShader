﻿Shader "FX/Censor (Masked Cutout)"
{
	Properties
	{
		[NoScaleOffset]_MainTex ("Mask (Alpha)", 2D) = "white" {}
		_Pixelation ("Pixelation", Range(1, 256)) = 6
	}
	SubShader
	{
		Tags { "RenderType"="Cutout"
		"Queue" = "Overlay" }
		LOD 100

		GrabPass
        {
			//Do a grab pass
        }

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			
			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float4 vertex : SV_POSITION;
				float2 uv : TEXCOORD0;
				float4 screenUV : TEXCOORD1;
			};

			sampler2D _GrabTexture;
			sampler2D _MainTex;
			float _Pixelation;
			
			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				#ifdef UNITY_SINGLE_PASS_STEREO
				o.uv = UnityStereoScreenSpaceUVAdjust(v.uv, _MainTex);
				#else
				o.uv = v.uv;
				#endif

				#if !UNITY_UV_STARTS_AT_TOP
				o.uv.y = 1 - o.uv.y;
				#endif

				o.screenUV = ComputeGrabScreenPos(o.vertex);
				return o;
			}

			// Gold Noise ©2015 dcerisano@standard3d.com
			// - based on the Golden Ratio
			// - uniform normalized distribution
			// - fastest static noise generator function (also runs at low precision)
			// - use with indicated seeding method. 
			const float PHI = 1.61803398874989484820459;  // Φ = Golden Ratio
			float gold_noise(float2 xy, float seed){
				return frac(tan(distance(xy*PHI, xy)*seed)*xy.x);
			}

			fixed4 frag (v2f i) : SV_Target
			{
				fixed mask = tex2D(_MainTex, i.uv).a;
				clip(mask - 0.5);

				float4 screenUV = i.screenUV / i.screenUV.w;
				//screenUV.z = ( UNITY_NEAR_CLIP_VALUE >= 0 ) ? screenUV.z : screenUV.z * 0.5 + 0.5;
				float4 pixelUV = half4(
					((int)(screenUV.x * _ScreenParams.x / _Pixelation) + 0.5) / _ScreenParams.x * _Pixelation,
					((int)(screenUV.y * _ScreenParams.y / _Pixelation) + 0.5) / _ScreenParams.y * _Pixelation,
					screenUV.z, screenUV.w
				);

				half4 screenColorPixelized = tex2Dproj(_GrabTexture, pixelUV);

				screenColorPixelized.xyz += gold_noise(_Time.y * 0.001 + pixelUV.xy * 8.0, 16.0) * 0.025;

				return screenColorPixelized;
			}
			ENDCG
		}
	}
}
